class Airplane:
    def __init__(self, name, number_of_seats):
        self.name = name
        self.number_of_seats = number_of_seats
        self.total_distance_flown = 0
        self.number_of_occupied_seats = 0

    def fly(self, distance):
        self.total_distance_flown += distance

    def is_service_required(self):
        if self.total_distance_flown > 10000:
            return True
        else:
            return False

    def board_passengers(self, number_of_passengers):
        if number_of_passengers > self.get_available_seats():
            self.number_of_occupied_seats = self.number_of_seats

        else:
            self.number_of_occupied_seats += number_of_passengers

            print(f"Boarded {number_of_passengers} passengers.")

    def get_available_seats(self):
        return self.number_of_seats - self.number_of_occupied_seats


if __name__ == '__main__':
    airplane1 = Airplane("Airplane1", 200)
    airplane2 = Airplane("Airplane2", 300)
    print(airplane1.name, airplane1.number_of_seats)
    airplane1.fly(10000)
    print(airplane1.is_service_required())
    airplane1.board_passengers(190)
    print(airplane1.number_of_occupied_seats)
    print(airplane1.get_available_seats())
